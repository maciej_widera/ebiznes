FROM debian

LABEL author="Maciej Widera <maciej.widera@student.uj.edu.pl>"

WORKDIR /app

ADD . /app

RUN \
    apt-get update && apt-get install -y \
        wget \
        default-jre-headless \
        git-core && \
    wget \
        https://downloads.lightbend.com/scala/2.12.4/scala-2.12.4.deb \
        https://dl.bintray.com/sbt/debian/sbt-1.1.0.deb && \
    dpkg -i \
        scala-2.12.4.deb \
        sbt-1.1.0.deb &&\
    rm \
        sbt-1.1.0.deb \ 
        scala-2.12.4.deb && \
    apt-get update && \
    apt-get install -y sbt && \
    git clone https://maciej_widera@bitbucket.org/maciej_widera/ebiznes.git && \
    sbt update && \
    apt-get clean

EXPOSE 80
